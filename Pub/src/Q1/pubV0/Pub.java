package Q1.pubV0;


class Drink {
    private String drink;
    private int price;
    private Boolean studentDiscount;
    private Boolean limited;
    private int limitPerOrder;

    Drink(String drink, int price, Boolean studentDiscount){
        this.drink = drink;
        this.price = price;
        this.studentDiscount = studentDiscount;
        this.limited = false;
    }

    Drink(String drink, int price, Boolean studentDiscount, int limitPerOrder){
        this.drink = drink;
        this.price = price;
        this.studentDiscount = studentDiscount;
        this.limited = true;
        this.limitPerOrder = limitPerOrder;
    }

    int getPrice(){
        return price;
    }

    Boolean hasStudentDiscount(){
        return studentDiscount;
    }

    Boolean isLimited(){
        return limited;
    }

    int getLimitPerOrder(){
        return limitPerOrder;
    }
}


class DrinkFactory{
    Drink getDrink(String drink){
        int price;

        switch (drink){
            // Branded drinks
            case "hansa":
                return new Drink(drink, 74, true);
            case "grans":
                return new Drink(drink, 103, true);
            case "strongbow":
                return new Drink(drink, 110, true);

            // Artesanal drinks
            case "gt":
                price = ingredient6() + ingredient5() + ingredient4();
                return new Drink(drink, price, false, 2);
            case "bacardi_special":
                price = ingredient6() + ingredient1() + ingredient2() + ingredient3();
                return new Drink(drink, price, false, 2);

            // If there is not such drink
            default:
                throw new RuntimeException("No such drink exists");
        }
    }

    //one unit of rum
    private int ingredient1() {
        return 65;
    }

    //one unit of grenadine
    private int ingredient2() {
        return 10;
    }

    //one unit of lime juice
    private int ingredient3() {
        return 10;
    }

    //one unit of green stuff
    private int ingredient4() {
        return 10;
    }

    //one unit of tonic water
    private int ingredient5() {
        return 20;
    }

    //one unit of gin
    private int ingredient6() {
        return 85;
    }
}


public class Pub {

    private DrinkFactory factory;

    public Pub(){
        factory = new DrinkFactory();
    }

    public int computeCost(String name, boolean student, int amount) {

        Drink drink = factory.getDrink(name);

        if (drink.isLimited() && amount > drink.getLimitPerOrder()) {
            throw new RuntimeException("Too many drinks, max " + String.valueOf(drink.getLimitPerOrder()) + ".");
        }

        int price = drink.getPrice();
        if (student && drink.hasStudentDiscount()) {
            price = price - price/10;
        }

        return price*amount;
    }
}
